<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<div class="blog-wrap">
	<header class="post-head">
		<a class="button back" href="<?php echo get_site_url(); ?>/journal/">Back to all posts</a>
		<h1><?php the_title(); ?></h1>
		<p><?php the_date('F j Y'); ?></p>
		<?php	
		$author_id = get_the_author_meta('ID');
		?>
		<p><strong><?php the_author(); ?><?php if (get_field('position', 'user_'. $author_id)) : ?>, <?php the_field('position', 'user_'. $author_id); ?><?php endif; ?></strong></p>
		<div>
			<?php 
				$post_url = get_permalink();
				$post_content = get_the_excerpt();
			?>
			<a target="_blank" href="https://twitter.com/home?status=<?php echo $post_content; echo $post_url; ?>" title="Share on Twitter">
				<svg>
					<use xlink:href="#twitter" />
				</svg>
			</a>
			<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_url; ?>&summary=<?php echo $post_content; ?>" title="Share on LinkedIn">
				<svg>
					<use xlink:href="#linkedin-2" />
				</svg>
			</a>
			<a target="_blank" href="mailto:" title="Share via email">
				<svg>
					<use xlink:href="#email" />
				</svg>
			</a>
		</div>
	</header>

	<aside>
		<h6>Related Posts</h6>
		<hr>
		<?php
			$categories = get_the_category();
			$category_id = $categories[0]->cat_ID;
		?>
		<section class="related-posts">
			<?php
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => 3,
					'order' => 'ASC',
					'post__not_in' => array( $post->ID ),
					'cat' => $category_id,
				);
				$loop = new WP_Query( $args );
				?>
				<?php while ( $loop->have_posts() ) : $loop->the_post();?>
					<article class="archive-result <?php echo $post_type; ?>">
						<header>
							<?php	
							$categories = get_the_category();
							if ( ! empty( $categories ) ) {
								echo '<a class="category" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
							} ?>
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
						</header>
						<div class="entry-content">
							<?php the_excerpt(); ?>
						</div>
					</article>
				<?php endwhile;?>
				<?php wp_reset_query(); ?>
		</section>
	</aside>

	<main id="main-content">
		<article>
			<?php the_content(); ?>
			<hr>
			<div class="post-end">
				<div>
					<a target="_blank" href="https://twitter.com/home?status=<?php echo $post_content; echo $post_url; ?>" title="Share on Twitter">
						<svg>
							<use xlink:href="#twitter" />
						</svg>
					</a>
					<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_url; ?>&summary=<?php echo $post_content; ?>" title="Share on LinkedIn">
						<svg>
							<use xlink:href="#linkedin-2" />
						</svg>
					</a>
					<a target="_blank" href="mailto:" title="Share via email">
						<svg>
							<use xlink:href="#email" />
						</svg>
					</a>
				</div>
				<?php $tags = get_the_tags($post->ID); ?>
				<?php if ($tags) : ?>
				<p>TAGS: <?php foreach($tags as $tag) :  ?><a href="<?php bloginfo('url');?>/tag/<?php print_r($tag->slug);?>"><?php print_r($tag->name); ?></a><span>,</span> <?php endforeach; ?></p>
				<?php endif; ?>
			</div>
		</article>
	</main>
</div>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>