<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach Nine
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<div class="title">
			<h1 class="h2">Every Brand’s Guide to Slang, Memes, and Internet Speak</h1>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/InternetSpeak-Header.png" alt="Woman Climbing Stairs to Internet Browser"/>
		</div>
		<p class="large-p">Spending time on the internet can feel like navigating two different languages and two different cultures. But if you’re a brand looking to connect with younger audiences, you need to be fluent in both.</p>
		<p>We at Skidmore know how to tap into the weird and wild ways of the internet, especially as it relates to brands who want to talk the talk in authentic ways. In addition to providing <a href="https://www.google.com/" target="_blank">an internet dictionary</a>, we’re here to be your tour guides through the history, culture and—yes—language of this brave new world.</p>
		<section class="black">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/InternetSpeak-DigitalNatives.png" alt="House of Digital Natives Full of Gen Z and Millennials  Using Their Phones"/>
			<h3>Meet the digital natives</h3>
			<p>When it comes to finding today’s internet locals, look no further than Millennials and Generation Z. These two groups, ranging from their early teens to mid-thirties, were the first generations to grow up with the internet as a ubiquitous tool in their everyday life. (They’re often called “digital natives” for that very reason.)</p>
			<p>Millennials shaped the way we communicate online by exploring fonts, colors, and emoticons in their early AIM days. Their way of communicating online continued to evolve into a mix of non-standard written word and visual rhetoric—better known as slang and memes—as they moved into adulthood. By the time Gen Z came around and started to leave their own mark on internet culture, these parts of speech were prevalent and established enough to become part of Gen Z’s digital and real lives.</p>
		</section>
		<h3>Parts of (Internet) Speech</h3>
		<p>There are three key methods of communication that differentiate Internet Speak from standard English: slang (text-based), memes (visual), and text style (a blend of visual and text-based communication). To understand how the internet uses these tools, you have to know the function it serves within the language.</p>
		<div class="three-circles">
			<img class="desktop" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/PartsofSpeech@2x.png" alt="Book of Internet Speak"/>
			<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/InternetSpeak-Inline.png" alt="Book of Internet Speak"/>
			<div>
				<h5>Slang</h5>
				<p>Invented, repurposed, or shortened words; characteristically more playful, vivid, and ephemeral.</p>
			</div>
			<div>
				<h5>Memes</h5>
				<p>Visual interpretations of a feeling or emotion meant to accompany text; spreads based on relatability.</p>
			</div>
			<div>
				<h5>Style</h5>
				<p>Non-standard grammar usage that mimics the tone of spoken word or expressions via plain text.</p>
			</div>
		</div>
		<p>What makes Internet Speak potentially confusing is how it lives in tandem with standard English. Each piece of this new language builds of the vocabulary of spoken English and the shared experiences of its users to create a shorthand for complex emotions—similar to how tone of voice or body language would affect a face-to-face conversation.</p>
		<section class="mint">
			<h3>Use slang and memes in your brand voice </h3>
			<p>Once you’re semi-fluent in understanding Internet Speak, the next step to total comprehension is to participate in the culture. Whether you’re using memes in marketing or trying to inject a more youthful tone into your brand voice, there are a few travel tips that will help you look less like an online tourist and more like someone the tourists ask for directions.</p>
		</section>
		<div class="flex tip">
			<div class="pro-tip">
				<h5>Find the right translator</h5>
				<p>Those who already participate in the online culture of your intended audience (hi, Millennials and Gen Zers) make great local guides to the hidden gems of the “neighborhood.” They have an innate sense of your audience’s online presence and can naturally pull their language into your brand’s communication.</p>
			</div>
			<div>
				<p>Using slang, memes, and other internet communication styles isn’t as simple as switching out a word with its newer synonym. For brands to create a true connection with their audience, they need to understand the language as it develops. Because when a slang word or meme saturates public knowledge enough to be “recognized” by large-scale media organizations, it often means the unit of speech has lost its insider status and appeal, making it easier to see who is trying to be cool vs. who actually knows what they’re talking about.</p>
				<p>And, like it’s spoken counterpart, this digital language also has its own “regional dialects.” An individual’s exposure to a certain unit of Internet Speak is based on where they spend their time on the internet. By straying off the beaten path, you’ll find how your audience uses any particular word or meme in their own way while building an authentic vocabulary of Internet Speak for your brand.</p>
				<h3>Practice cultural sensitivity</h3>
				<p>Many popular words and memes originate in underrepresented communities; most notably they are plucked from American Vernacular English (AAVE) or the queer community. A person adopting this slang is one thing, but brands are a whole different matter. Unless a brand has strong connections to the community, its use of the language—no matter how viral—comes off as out of touch at best and exploitative at the worst. An inauthentic use of slang from a brand that only understands its surface-level meaning is a vain attempt to appear current to a young audience.</p>
			</div>
		</div>
		<section class="black">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/InternetSpeak-buildings.png" alt="Car Driving Local Around City and Social Media"/>
			<h3>Live like a local</h3>
			<p>At the end of the day, internet communication styles are simply new ways to connect with each other. Whether it’s bonding over a meme, using the latest slang word ironically (but not too ironically), or trying to be in your feelings in the group chat, language is evolving with the times—and brands are expected to keep up.</p>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>