<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach Two
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<div class="title">
			<h1 class="h2"><span>The 4 </span><span>Parts of </span><span>Every Brand</span></h1>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/01-Anatomy-Hero.jpg" alt="Parts of a Brand"/>
		</div>
		<p><strong>Say the word “brand” and everyone from the CMO to the summer intern has a strong conviction about it. Your CEO says it’s a promise, your Graphic Designer says it’s your logo, your Marketing Director says it’s your latest ad campaign, your Events Coordinator says it’s the consumer experience at your CES activation. Ad infinitum.</strong></p>
		<p>Let’s clear out the clutter. Your “capital-B” Brand is none of those things, individually—it’s all of those things, collectively.</p>
		<div class="flex three">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/02A-Anatomy-Person.jpg" alt="Man Perceptions"/>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/02B-Anatomy-Person-2.jpg" alt="Woman Perceptions"/>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/02C-Anatomy-Person-3.jpg" alt="Dog Perceptions"/>
		</div>
		<h3>What is your brand?</h3>
		<p>Brand = Perception</p>
		<p>Your Brand is the perception of your People, Product, Process and Public Face—in that order. But, as they say, perception is reality. Regardless of what you think about your organization and its products, and regardless of what you say about yourself, your Brand is what people perceive about your…</p>
		<ol>
			<li><h3>People</h3> The culture, character and values of the people in your organization</li>
			<li><h3>Product</h3> The product, service, or experience consumers buy from you and the quality of it</li>
			<li><h3>Process</h3> The way your People deliver your Product</li>
			<li><h3>Public Face</h3> The promises you’re making about your People, Product, and Process</li>
		</ol>
		<p>The tricky bit is that perception is subjective. No two consumer journeys with your Brand are the same, so no two consumers’ perception of you are the same. The thing that will keep you up at night is this truth: There exists a unique version of your Brand for every consumer who’s ever had any level of awareness or interaction with you. So let’s break down how each of facet of your organization impacts your Brand.</p>
		<div class="flex four">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/03-Anatomy-Pro-Tip.png" alt="Coal Turning into Diamond"/>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/03-Anatomy-Pro-Tip-2.png" alt="Coal Turning into Diamond"/>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/03-Anatomy-Pro-Tip-3.png" alt="Coal Turning into Diamond"/>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/03-Anatomy-Pro-Tip-4.png" alt="Coal Turning into Diamond"/>
		</div>
		<div class="pro-tip">
			<div>
				<h5>Pro Tip</h5>
				<p>If Brand is perception, then Branding is the work of creating that perception. While Brand is simply something that is, Branding is something that you can actually do. Spend your resources on Branding and watch the quality of your Brand improve.</p>
			</div>
		</div>
		<div class="flex">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/04-Anatomy-People.jpg" alt="Number One"/>
			<div>
				<h3>People</h3>
				<p>The first and most impactful part of your brand is your People. This includes everyone in your organization from “top” to “bottom”—consumer-facing or not. The sum total of the character, attitudes and values of your staff has primacy in creating Brand. </p>
				<p>The quality of your people will manifest itself in every other facet of your Brand; it cannot be hidden. If you have lousy people, you have a lousy Brand. If you have truly good people, you are destined to succeed. What kind of people do you have?</p>
				<p>As with everything else, leadership matters in building a quality Brand. The saying is true: “Every organization looks like its leaders.” This is why, when we help you create your Public Face, we’re adamant about working with your leaders to find and document their Core Purpose. </p>
				<p>When leaders clearly understand and articulate the organization’s Why, it creates a lighthouse. This lighthouse unites staff with a singular vision, it fills the hesitant with confidence and it guides consumers safely to its shore in a hurricane of competing messages.</p>
			</div>
		</div>
		<div class="flex">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/05-Anatomy-Product.jpg" alt="Number Two"/>
			<div>
				<h3>Product</h3>
				<p>The second most impactful part of your Brand is Product. Perhaps your Product is an actual thing, perhaps it’s a service or an experience, or perhaps it’s a combination of these. In any case, your Product is something of value that people are buying from you. They’re handing over their hard-earned cash so whatever it is you’re selling, it better add value to their lives!</p>
				<p>When your Product is great—even if your Public Face is weak—you’ll often find success simply because people love what it’s done for them. They’ll buy it again. They’ll tell their friends and family about it. They’ll be happy to try new products you’re offering.</p>
				<p>But if your Product lets them down, you won’t get a second chance. In other words: No amount of slick advertising can save crappy Product.</p>
			</div>
		</div>
		<div class="flex">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/06-Anatomy-Process.jpg" alt="Number Three"/>
			<div>
				<h3>Process</h3>
				<p>Process is the most overlooked part of Brand. Process is how your People deliver your Product. It’s every single part of that experience, from operations to customer service to ethical sourcing to shipping and delivery.</p>
				<p>Process is also one of the hardest brand facets to improve because it covers so may soft-skill, wide-ranging touchpoints. Where do you begin? Start with an eye-opening, human-centered research exercise. You’re likely to be a little blind to what it’s really like to be a consumer who interacts with you, or an employee who’s on the front lines. We often lead our projects with listening sessions, Customer Journey Maps, and Design Thinking workshops to help uncover “what’s really going on” with an organization’s Process—and how you might improve it.</p>
			</div>
		</div>
		<div class="flex">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/07-Anatomy-PubliceFace.jpg" alt="Number Four"/>
			<div>
				<h3>Public Face</h3>
				<p>Your Public Face is the least impactful part of your Brand because it is simply a promise. The Public Face of your Brand is any communication that you put out into the marketplace telling people what they should expect from an interaction with your People, the quality of your Product, and the feel of your Process.</p>
				<p>Where many organizations go wrong is in making promises that their People, Product, and Process haven’t committed to paying off. So the safest bet is to develop a Public Face that promises only what your People, Product and Process can actually deliver.</p>
				<p>Safest, yep, but if you want to transcend “safe” and do something great, read on... </p>
			</div>
		</div>
		<br>
		<br>
		<h3>Follow our lead</h3>
		<p>Understand that in almost every case, Brand Follows Operations. If Operations is comprised of your People, Product, and Process, and Public Face is simply an accurate reflection of those things, then Operations is leading the dance. In some cases, however, Brand can lead Operations.</p>
		<p>Brand leads Operations by being inspirational to the organization itself. This happens when Public Face says, “We’re committed to being X, so let’s work hard every minute of every day to make that a reality for our customers.” </p>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/08-Anatomy-Conclusion.jpg" alt="Young Boy Shapes Brand"/>
		<p>It’s a bold move because the organization is making promises today that it’s not totally sure how to pay off tomorrow. But when the organization has committed to closing that gap—and delivering for its customers no matter what—it creates a sense of urgency where creativity reigns and something truly good and different can be brought into the world.</p>
	</article>
</main>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>