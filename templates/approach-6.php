<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach Six
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<h1 class="h2">The Fine Art of Being Different</h1>
		<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Differenting-Hero-HotDogs@2x-Mobile.jpg" alt="Green Doodle Character Stands Out from Crowd of Black Doodles"/>
		<img class="desktop" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Differenting-Hero-HotDogs@2x-Desktop.jpg" alt="Green Doodle Character Stands Out from Crowd of Black Doodles"/>
		<p class="large-p">You’ve heard of “lawyer-ing up,” right? The strategy of hiring ever-more expensive lawyers than your opponent in order to help you fight an upcoming legal battle. There’s a similar phenomenon in the world of product differentiation: feature-ing up. It goes like this:</p>
		<ol>
			<li>A brand begins to dominate a category with a superior product, becoming the de-facto leader.</li>
			<li>Follower brands feel forced to say that they’re just as good, or better, <em>on the same characteristics</em> as the leader.</li>
			<li>Leader brand then innovates and/or begins marketing on new characteristics.</li>
			<li>Follower brands…follow.</li>
			<li>Infinitely repeat steps 3 and 4 until the category is a junk drawer of the exact same product, with each brand extolling the immeasurable importance of their own petty differentiators.</li>
		</ol>
		<p>There is a way to transcend this escalating arms race. One that catapults a brand out of the morass and directly into the hearts of consumers. We call it “differenting.”</p>
		<p>And yes, we made up a new word. </p>
		<p><strong>differenting (di•frent•ing)</strong> 1. the act of creating a meaningful emotional connection with your audience by using characteristics that are authentic, positive and unmatched; 2. the winning strategy in product differentiation battles</p>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Differenting-Step1-Mirror@2x-Desktop.jpg" alt="Doodle Character Looks in Mirror"/>
		<h3>Get real</h3>
		<p>The first step of differenting is to identify what is honest and true about your product. In what ways is your product really good, and in what ways is it lacking? Ultimately, you need to determine your most authentic characteristics.</p>
		<p>Introspection is a team sport; don’t do this by yourself. You have too many blind spots about your product, so you need to gather unvarnished intel from those who know you best: users. Go out into the field and talk with three different kinds of people. You should spend time with the lovers who give you NPS scores of 10, haters who won’t touch you with a ten-foot pole, and the “meh” crowd that is mind-numbingly indifferent.</p>
		<p>Listen and observe. Each group will bring you polarizing but fascinating new insights you’ve never even considered. And because the whole is greater than the sum of its parts, the collective wisdom you gain will paint a clear picture of what’s truest about you.</p>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Differenting-Step2-Smile@2x-Desktop.jpg" alt="Doodle Character Playing Catch with Ball Over Twisting Body"/>
		<h3>Make ‘em laugh</h3>
		<p>It’s time to get positive. Not the kind of “positive” that’s an exaggerated recitation of your strengths. (Remember, your competition is already doing that!) Instead, we want you to birth fresh positivity into the world.</p>
		<p>It’s kind of a dark place out there, with too few lighthouses guiding the way and not enough warm fireplaces beating off the chill. Products win when they promise—and deliver—goodness that truly adds value and makes someone’s day just that much better. Brands that succeed are those which communicate something that <em>feels</em> good to their audience.</p>
		<p>When you leverage what is authentic to you, how might your product connect with you audience in a <em>positive</em> way?</p>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Differenting-Step3-Twisty@2x-Desktop.jpg" alt="Doodle Character with Twisted and Tangled Body"/>
		<h3>And now for something completely different</h3>
		<p>You’re being authentic and positive, but that just makes you a worthy competitor in a well-established game. You need to play a different game.</p>
		<p>Here’s the scenario: 99% of brands are challengers in their category, looking to steal share from the leader by picking a fight for the same position. The leader is entrenched in this battle, doubling down on their positioning, and using their size and scale (and new innovation studio!) to continually win. But please see their implicit weakness: the leader cannot adjust quickly to unexpected threats. <em>You</em> can be the unexpected threat by being unmatched.</p>
		<p>Being unmatched is the third and most important characteristic of differenting. Leader and followers use product differentiation battles to define the rules of engagement for this category. Therefore, consumers expect a laundry list of things that X category of products will do for them. You must find and broadcast something that transcends the category. What is emotionally resonant and unmatched about your product that the leader (and followers) cannot compete with and which goes beyond the consumer’s expected norms and complacencies about the category?</p>
		<p>Consumers have unmet and under-recognized emotional needs in any category. To be unmatched is to uncover these needs and redefine the expectations of what the category can do for them.</p>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Differenting-Endcap-Hill@2x-Desktop.jpg" alt="Doodle Character with Twisted Body"/>
		<section>
			<h3>Finding the balance</h3>
			<p>A product can be authentic …but on its own, that’s not very compelling.</p>
			<p>A product can be positive …but the warm-and-fuzzies only go so far.</p>
			<p>A product can be unmatched …but runs the risk of its superior characteristic coming off as arrogance.</p>
			<p>To practice the art of differenting is to find the intersection of what’s authentic to you, brings something positive into the world, and is unmatched in its ability to connect with consumers’ unmet emotional needs. Redefine the art of product differentiation, and you’ll succeed by redefining your category.</p>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>