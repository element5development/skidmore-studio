<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach Eight
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<section class="gray">
			<h1 class="h2">What’s in a<br>(Good) Name?</h1>
			<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Naming-Rose-Header@2x-Mobile.jpg" alt="Naming Rose Character"/>
		</section>
		<p class="large-p">Whether you’re naming an organization, event, sub-brand, or product, the naming process is one of the most harrowing challenges for any internal team. So much of the final outcome hinges on difficult-to-control factors like varying stakeholder opinions, legal viability, and an ever-shrinking pool of available names.</p>
		<p>Still, naming remains a critical challenge worth investing in: A good name will become the (often-quietly) beating heart of your brand. It can lead, point to, augment the rest of your branding efforts in a million ways big and small. And, as people’s attention spans shrink along with the practice of longform reading, a name remains the central—if only—piece of your brand that retains the richness and texture only written language can provide.</p>
		<section class="mint">
			<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Naming-Branch1@2x-Mobile.png" alt="Naming Rose Thorns Leaves"/>
			<h3>The qualities of a good name</h3>
			<p>Every naming project that comes through our door is created through human-centered design. That means we get to understand the problem/opportunity, the people it affects, and spend a lot of time shaping a collective go-forward strategy—all before lifting a pen to brainstorm.</p>
			<p>This process helps us and our client partners establish broader goals, hopes, dreams, and limitations for the future brand/product/event that transcend its name. From there, stakeholders enter the naming process with confidence and objectivity.</p>
			<p>Beyond the specific goals of X product or X brand, a good name pretty much always possesses these four qualities:</p>
			<br>
			<br>
		</section>
		<section class="mint">
			<ol>
				<li><h3>It’s different.</h3>A name like it doesn’t exist within your competitive set, industry, or brands your target consumers use. Naming experts use the term “special wrongness” to describe a name with a certain amount of “huh?” to it. The need to be different is as creative-oriented as it is legal.</li>
				<li><h3>It’s ownable.</h3>Yes, we mean legally, but we also mean symbolically. Is this name something your brand can really claim? While we’ll never advocate for literal names, a good name will establish some sort of connection to your product or service, be it spiritual, symbolic, or phonetic. Even the lack of connection can be a connection…to a quirky brand personality!</li>
				<li><h3>It’s sticky.</h3>Creating a name that’s one-of-a-kind is easy enough. The trick is to do this while maintaining customer-friendly spelling, pronunciation, and recall. It’s got to stick in customers’ brains. Sensational spelling and made-up words, while extremely convenient for your legal team, can fail miserably on the sticky test.</li>
				<li><h3>It’s emotional.</h3>The best, most potent consumer-facing names strike an emotional note. They’ve got real <em>feeling</em> to them. Nike suggests glory and super-humanity. With Mailchimp, it’s instant levity. La Croix gives us fancy bordering on kitsch. Finding a name with feeling is as much of an art as it is a science (see: <a href="https://en.wikipedia.org/wiki/Sound_symbolism" target="_blank">sound symbolism</a>), and requires a rock-solid grasp on the zeitgeist.</li>
			</ol>
			<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Naming-Branch2@2x-Mobile.png" alt="Naming Rose Thorns Leaves"/>
		</section>
		<br>
		<br>
		<h3>Getting started</h3>
		<p>At Skidmore, we begin the creative phase of naming projects with rapid generation. Our strategists and copywriters lead exercises ranging from micro-fiction-writing to word-association games to word-hunting in history hooks and poetry. One of our most effective naming exercises is a timed, stream-of-conscious game called “prompt, write, pass,” where a group of creatives generate imagery from a one-word prompt and pass it to the left. This is repeated until everyone has built upon each other’s ideas, one relevant prompt after the next.</p>
		<p>Once a bunch of ideas are generated, the hard part begins: Culling down, curating, and assembling potential names. Here, our strategists and writers focus on picking out the words, bits, and pieces—sometimes just a letter, syllable, or sound—with potential. These names are polished and vetting through the four qualities of a good name. If they pass, they require a few additional considerations:</p>
		<p>
			<a href="https://www.google.com/" target="_blank">Google:</a> What’s out there with that word or phrase currently?<br>
			<a href="https://www.uspto.gov/trademarks-application-process/search-trademark-database" target="_blank">USPTO TESS:</a> Is there an existing trademark on the name, or something like it?<br>
			<a href="https://www.godaddy.com/" target="_blank">Domain availability:</a> Is there a solid URL option, if this name is chosen?<br>
			<strong>Legal counsel:</strong> If you have an in-house resource, what’s their risk tolerance? Might you get a second opinion?<br>
		</p>
		<br>
		<h3>Some final reminders</h3>
		<p>Truth be told, all creative work is somewhat subjective. However, when it comes to naming, which is so often an early task in a branding project, business leaders should strive to be as objective as possible. Map back to your business goals and brand strategy. Remember that names grow on consumers over time. Remember that a name is only the first part of the fully realized brand you’re building.</p>
		<p>Finally, have the courage to make a bold choice! Few brands win by playing it safe.</p>
		<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Naming-Branch3@2x-Mobile.jpg" alt="Naming Rose Thorns Leaves"/>
	</article>
</main>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>