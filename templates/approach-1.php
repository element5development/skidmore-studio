<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach One
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<div class="black-bg">
			<div class="moon-header">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/01-Design-Thinking-Hero.jpg" alt="Space Earth Planets Stars"/>
				<h1 class="h2">How We Leverage<br>Design Thinking to Build Brands</h1>
			</div>
			<p><strong>Design Thinking is a framework that guides our work process. That’s sure to prompt a reaction, so let’s get a few things out of the way:</strong></p>
			<ol>
				<li>If you don’t know what Design Thinking is, <a href="https://www.ideou.com/pages/design-thinking" target="_blank">start here</a>.</li>
				<li>There are a thousand Design Thinking frameworks out there. Yes, we’re adding one more.</li>
				<li>Design Thinking alone won’t solve your problems. It has to be implemented by committed, talented people—that’s us, and people like us!</li>
				<li>Design Thinking is as old as the hills…</li>
			</ol>
			<p>…because, at its heart, Design Thinking is simply a process for solving problems with enduring solutions. The dirty little secret of Design Thinking is that great creative people have been looking at the world through this lens and solving problems this way for hundreds (thousands?) of years.</p>
			<p>Since we believe problem-solving must be human-centered, we practice Design Thinking. But we’ve custom-tailored a process to work best for the type of work we do: creative branding. It comes in three parts.</p>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/02-Design-Thinking-Disco-Strat-Creative.jpg" alt="Design Thinking Stages: Discovery, Strategy, Creative" />
			<div class="flex">
				<div>
					<h3>Discovery</h3>
					<p><strong>Discovery is wide-eyed exploration, understanding the landscape of the problem/opportunity through the eyes of the people experiencing that problem/opportunity.</strong></p>
					<p>There’s an old saying, “You never really know a person till you’ve walked a mile in their moccasins.” So we walk the Moccasin Mile: we talk to people, we observe their actions, we do secondary research to see what others have uncovered in past explorations. And because our work is branding, we focus heavily on consumers as well as the competition—analyzing how competitors are attempting to solve this problem/opportunity, too.</p>
					<p>What comes out of this Discovery work is intelligence and insights. Intelligence is the clear facts and unvarnished truths that grow the collective knowledge base of Skidmore and our Client Partners. The insights are the revelations and “aha!” moments we bring our Client Partners in understanding their audience and beginning to see new and better ways to connect with them.</p>
					<p>When the Discovery phase is complete—though our process of discovering never is—we’ve established enough shared understanding between project stakeholders to dive into the next phase.</p>
				</div>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/03-Design-Thinking-Discovery.jpg" alt="Moon Mountain Design Thinking Discovery" />
			</div>
			<div class="flex">
				<div>
					<h3>Strategy</h3>
					<p><strong>Leveraging Discovery’s intelligence and audience insights, Strategy is the process of developing shared language, plans, and tools to solve the real problem for real people.</strong></p>
					<p>The first step is to confirm that the problem we originally set out to solve is actually the correct problem. (Discovery confirms this.) Once we’ve confirmed or redefined the real problem, we create strategy in the form of ideas and guidelines written to help a specific group of people.</p>
					<p>As a human-centered creative shop, the most important part of our Strategy phase is the pursuit of a clearly defined and well-understood audience. This isn’t a “demographic target market segment”—this is real, flesh-and-blood people named Theresa and Erica and Jorge whose stories we learned in Discovery and whose real-life needs, problems, hopes, and dreams are the focus of our strategies.</p>
				</div>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/04-Design-Thinking-Strategy.jpg" alt="Astronaut Helmet Book Glove Food" />
			</div>
			<div class="flex">
				<div>
					<h3>Creative</h3>
					<p><strong>With the landscape fully explored (Discovery) and real problems being solved for real people (Strategy), we enter the Creative phase with confidence. The winning approach is to move fast, generate a lot of great ideas, and work together to narrow in on a single solution.</strong></p>
					<p>We move fast because we are deeply skilled in our craft (strategy, messaging, graphic design) and work in rapid iterations. We move quickly from prototype to prototype, learning key insights along the way, refining and perfecting our creative output with—of course—ample collaboration and feedback.</p>
					<p>We work together in three important ways: First, all of our internal project teams are cross-functional, providing a wide range of skilled teammates to bounce ideas off of. Second, Skidmore has developed a rhythm of group-wide critiques and feedback sessions that generate new and important inputs from out-of-project perspectives. Third and lastly, the project team is comprised of both Skidmorvians and Client Partners. We bring branding expertise and our Client Partners bring expertise that can only come from the day-to-day intimacy they maintain in their particular area of business.</p>
				</div>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/05-Design-Thinking-Creative.jpg" alt="Space Shuttle Take Off" />
			</div>
		</div>
		<div class="white-bg">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/06-Design-Thinking-Conclusion.jpg" alt="UFO Alien Cow Abduction" />
			<h3>The truth is out there</h3>
			<p>We believe in our Design Thinking framework because, well, it works! It works because it’s the natural flow of a strategic creative process. It works because it’s repeatable and predictable. It works because real people are impacted by and served by the bullseye solutions we create. It works because it generates value and results for our Client Partners time and again.</p>
			<p>And it works because it makes beautiful, unexpected, and enduring brand connections.</p>
		</div>
	</article>
</main>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>