<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach Seven
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<h1 class="h2">What Makes a<br>Great Logo</h1>
		<h5>(and How to Know You Need a New One)</h5>
		<img class="desktop" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Ski_CC_Logo_Header.png" alt="Shirt Frames Matchbook Connected by Line"/>
		<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Ski_CC_Logo_header_mobile@2x.png" alt="Shirt Frames Matchbook Connected by Line"/>
		<p class="large-p">A logo can feel a lot like a last name. Something that’s been handed down over generations, its original meaning lost in time.</p>
		<p>Maybe you feel compelled to keep it, because it’s the way it’s always been. Maybe you’re afraid to shake things up. Afraid to break continuity with the past. Afraid of what your customers will think, what the stakeholders will want, or what the internet will say (and yes: they will have a lot to say). It doesn’t need to be that way.</p>
		<p>Yes, brand is more than a logo. As strategy-driven branders, we believe that more than the next creative agency. But in an increasingly design-savvy world, a weak logo is going to create compounding problems over time. If you’re struggling to attract new talent, launch new products, or enter new markets, your logo might be betraying you.</p>
		<br>
		<img class="mobile overlap" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Ski_CC_Logo_mid_mobile@2x.png" alt="Black and White Frame with White Arrow"/>
		<section>
			<p>A great logo will exude balance, beauty, and rhythm. It will feel right every time you see it. While design is perhaps the most objective of art forms, one of the best metrics is still your gut. If your logo feels off, and you just can’t figure out why, here are a few questions to guide you.</p>
			<div>
				<h5>Is it confident?</h5>
				<p>Put your logo up against the competition. Pull your top five competitors’ logos and put them in a lineup at equal size. If your logo appears to be hiding in the background, it’s likely a problem. If you’re constantly asking your creative team to “make the logo bigger,” it’s a related symptom. A great logo will command its own weight regardless of size.</p>
				<h5>Is it memorable?</h5>
				<p>A great logo will be indelible. Like a great pop song, you’re going to need a hook. Something able to be etched into minds and hearts. This is one of the more abstract and seemingly subjective questions. If you’re stumped, try the t-shirt test: Put your logo on some swag, like a t-shirt, and see how quickly you run out of inventory. Is your swag is disappearing alarmingly fast? Congrats, you’re probably on the right track. If you’ve got mounds of tees your family won’t even take, you may have a problem.</p>
				<h5>Is it flexible?</h5>
				<p>In order be recognizable, brands need to create a repetitious and consistent visual experience for the viewer. Ironically, one of the best ways to do this is to be flexible. Your logo will likely have to live in shapes as small as favicon and as large as a billboard. How well your logo works in that space depends on its ability to flex...while still maintaining consistency. A telltale sign of inflexibility is when your marketing team is constantly wrestling with (and often forced to adjust) the size, shape, color across mediums and form factors.</p>
				<h5>Is it honest?</h5>
				<p>Does your logo accurately portray you, your products, and/or your services? Keep in mind, there’s no need to be literal here. It’s okay to make people think or simply feel. (It’d be a sad day if every coffeeshop had a steaming hot cup of coffee for a logo.) Still, every line, shape, and letter tell a story. What story is your logo telling?</p>
			</div>
		</section>
		<br>
		<p>Your logo may be the smallest part of your brand, but it speaks volumes. At its best, it gives your audience a taste of your personality. At its worst, it betrays everything you stand for. A good logo will drop some visual clues about what you do and who you are. Not everything about you, but the most essential. Are you trustworthy, bold, quiet, quirky, unexpected? Your logo should communicate a core truth, a nugget of personality, to your audience. If it’s not doing that, it might be time for a change.</p>
		<br>
		<div class="relative">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Ski_CC_Logo_heart@2x.png" alt="Heart with Yellow Chalk Looped Line"/>
		</div>
	</article>
</main>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>