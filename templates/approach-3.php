<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach Three
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<div class="title">
			<h1 class="h2">Stop, Hold, Close: <span>How to Win the Aisle</span></h1>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-StopHoldClose-1-Desktop.png" alt="My Jam Label on Jar Packaging"/>
		</div>
		<p><strong>As Captain Obvious will tell you, getting noticed on a retail shelf isn’t easy. The competition is stiff.</strong></p>
		<p>In the best-case scenario, the shopper arrives at the store thinking about your product specifically. But in a noisy media landscape where you and your competitors are all singing the same song, this is increasingly unlikely. So, assuming they arrive thinking about your category, you’ve got a few seconds to communicate a very similar (identical?) set of features and attributes...but better.</p>
		<p>Or, if your produce is an impulse buy, then you need to both attract uninterested eyeballs <em>and</em> stand out from the competition. Yikes!</p>
		<p>What’s a brand to do? Let us introduce you to the Stop-Hold-Close framework.</p>
		<div class="flex">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/StopHoldClose-2.gif" alt="Wine Bottle with Label"/>
			<div>
				<h3>In the blink of an eye</h3>
				<p>This applies to all CPGs (Consumer Packaged Goods), but as a quick example of how stiff retail competition is, we’ll focus on the grocery store shelf—arguably some of the hottest real estate in America! </p>
				<p>Did you know that in a grocery store no single product receives more than six-tenths of a second of attention as the shopper is scanning to find what they’re looking for? Did you also know that on average no single grocery category receives more than six seconds of attention as the shopper is making a final decision?</p>
				<p>If that seems unbelievably short, think about your own grocery shopping trips. You go in with your list, find your category, scan for the brand you’re accustomed to, put it in your cart, and move on. Rinse and repeat until 45 minutes and a hundred dollars later you’re out the door with a cart full of the same brands you bought last week. Why? Because unfamiliar brands failed to capitalize on the six-tenths of a second that they had your attention.</p>
			</div>
		</div>
		<section>
			<div>
				<h3>Stop (the madness)</h3>
				<p>Every retail shelf—not just in the grocery store—is a crazy-house of competing messages, logos, colors, product claims, etc., etc., etc. Your first job is to catch the shopper’s gaze, extending it past the six-tenths of a second time limit that they have with you.</p>
				<p>The Stop is a visual tactic. Something in the look of the packaging must make it stand apart from the competition and win the aisle. It must visually say, “Hey, I’m different. In this sea of clutter, you can still find something unique.” This is most successful when it’s communicating in the native visual language of a specific target audience—the colors, motifs and styles that make someone (subconsciously) think, “That’s my jam.”</p>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-StopHoldClose-3-Desktop.png" alt="Bottles and Jars with Labels on Retail Shelf"/>
			</div>
		</section>
		<h3 style="max-width: 730px;">Hold (me)</h3>
		<p style="max-width: 730px;">Congratulations, you’ve made it past the 1-second mark! Now what are you going to do!? The Hold is what you say to the shopper once you’ve grabbed their attention. While the Stop is largely visual, the Hold is largely messaging.</p>
		<p style="max-width: 730px;">Your Hold is the product features, benefits, or brand purpose that are unique in this category. And, since you’re going head-to-head with similar competitive products, you need to spend some careful time identifying the things that are truly (and authentically) unique or compelling about your product. As with the Stop, the Hold is most successful when it’s target-specific messaging—words that one particular type of shopper will find irresistible. As we like to say: There’s riches in the niches.</p>
		<div class="flex tip">
			<div class="pro-tip">
				<h5>PRO TIP</h5>
				<p>Never use the Close to repeat the Stop or the Hold. They already did their job. The Close is the opportunity to look, feel, or say something uniquely convincing. Use it!</p>
			</div>
			<div>
				<h3>Close (the sale)</h3>
				<p>Now that you have your product off the shelf and in a shopper’s hand, how are you going to make the final push into their cart? The Close is all the things you’ll do to communicate what the Stop and the Hold haven’t said yet. </p>
				<p>You’re in the shopper’s hand—assume they’ll turn the package. Leverage the other surfaces to tell them something new; have a hook. This could be anything from the texture of the packaging materials to additional product benefits to use cases that aren’t immediately apparent. Whatever it is, just get that product into the cart!</p>
			</div>
		</div>
	</article>
</main>

<div class="flex yellow">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-StopHoldClose-5-Desktop.png" alt="Toast, Bread Knife, and Heart Shaped Butter"/>
	<div>
		<h3>It’s all you</h3>
		<p>In the sea of sameness, the Stop-Hold-Close gives you a simple framework for creating product packaging that gets you that first trial of your product. After that, it’s what’s on the inside that counts. A strong Stop-Hold-Close means you get strong first-time sales. A strong product means you get strong repeat sales. For growth, both are critical.</p>
	</div>
</div>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>