<?php 
/*----------------------------------------------------------------*\

	Template Name: About

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/page-header'); ?>

<?php get_template_part('template-parts/sections/skills'); ?>

<main id="main-content">
	<article>
		<?php the_content(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/team'); ?>

<?php get_template_part('template-parts/sections/studio'); ?>

<?php get_template_part('template-parts/sections/values'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>