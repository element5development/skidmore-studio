<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach Four
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<div class="title">
			<h1 class="h2">Branding Over Marketing for Better ROI</h1>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/BrandvMarketing-V2-4.png" alt="Four Paper Airplanes Side By Side"/>
		</div>
		<br>
		<br>
		<br>
		<p class="large-p">We believe in the power of branding over marketing.</p>
		<p>Investing 60% of your budget on branding and 40% on marketing is the best way to generate measurable, predictable results that grow your business. We’ve seen—and the data backs us up—that any deviation from this roughly 60/40 formula dramatically reduces the effectiveness of your short-term marketing tactics and your long-term profitability.</p>
		<p>But before we unpack such a polarizing belief, let’s define terms. We often hear the words Branding and Marketing used interchangeably—and therefore very, very incorrectly. Here are two simple definitions.</p>
		<br>
		<br>
		<br>
		<section>
			<p><strong>Branding</strong> is the work of telling your audience that you exist and what you stand for.</p>
			<p><strong>Marketing</strong> is the work of convincing your audience to buy what you’re selling.</p>
		</section>
		<h3>Why 60/40 makes sense</h3>
		<p>The purpose of your branding work is to accomplish awareness-level goals—to make your audience aware of you. In the context of the traditional purchase funnel, your branding work is top-of-funnel activities. It pays long-term dividends, not short-term results. It continues to build your overall brand identity, sharing emotionally relevant messages with your audience, defining who you are and what you stand for.</p>
		<br>
		<p class="large-p">Placing a larger share of your investment (60%) in branding work does two important things:</p>
		<ol>
			<li>It introduces you to new customers, aka increased brand awareness.</li>
			<li>It keeps you top-of-mind for existing customers, aka repeat purchases.</li>
		</ol>
		<div class="relative">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/BrandvMarketing-V2-3.png" alt="Paper Airplane Crash"/>
		</div>
		<p>On the other hand, your marketing work operates on the opposite end of the purchase funnel, creating desire for your products and converting that desire into purchase actions and increased market share. It has a short life span and it generates short-term results. Marketing work is usually oriented around specific products and often communicates limited-time promotions. It is rationale-based messages about exactly what you have to offer and why people should buy from you.</p>
		<p>When paired with a solid investment in branding, this proportionally lower investment in marketing (40%) is more effective because it’s more focused: it directs your audience to specific purchase actions, and concentrates on conversion vs. introducing (or reintroducing) the brand.</p>
		<p>Both branding and marketing are essential to growing a healthy business. When you invest first in branding, you establish strong emotional connections with your audience, creating a firm foundation onto which you can add marketing messages. Your marketing, building off this balanced footing, then has the opportunity to layer the rationale—product details, offers, promotions—that convert your brand-aware audience into enthusiastic customers.</p>
		<br>
		<br>
		<br>
		<br>
		<br>
		<section>
			<p><strong>TL;DR Once people get to know you, they’re much more interested in buying from you.</strong></p>
		</section>
		<h3>Ok, but what if I don’t wanna?</h3>
		<p>The 60/40 split of branding and marketing is critical to solid growth. Deviating from this ratio means overinvesting in one side of the formula, dramatically cutting the effectiveness of the other side.</p>
		<p>Overinvesting in branding is akin to running around introducing yourself to lots and lots of people, but never actually making them aware that you have something good to offer them. Conversely, overinvesting in marketing is being “that guy” who’s always asking you to buy something from him but making you feel uncomfortable about it because you have no idea who he is or what he stands for. (Don’t be that guy. Actually, don’t be either of those guys.)</p>
		<br>
		<h3>Give it a go</h3>
		<p>We love watching our clients succeed. But what we love even more is the opportunity to help them grow their business with a proven formula that works. Committing to the clarity and simplicity of investing 60% in your branding and 40% in your marketing gives us all a road map to making sound business decisions that will create tangible results.</p>
		<br>
		<div class="relative">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/BrandvMarketing-V2-5.png" alt="Paper Airplane Landing"/>
		</div>
		<br>
	</article>
</main>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>