<?php 
/*----------------------------------------------------------------*\

	Template Name: Approach Five
	Template Post Type: approach

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<div class="title">
			<h1 class="h2">How to <span>Define Your Audience</span> Find Your People</h1>
			<img class="desktop" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-Audience-1-Desktop.png" alt="Sisyphus Leaning Against Boulder"/>
			<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-Audience-1-Mobile@2x.jpg" alt="Sisyphus Leaning Against Boulder"/>
		</div>
		<p class="large-p">Let’s get this out of the way: You actually don’t have an audience.</p>
		<p>Think about the word “audience.” It assumes you, the speaker, are on a stage or some elevated position. And it assumes that a crowd has willingly gathered around you—often paying money to do so—because they want to hear what you have to say. Does that sound like today’s branding and marketing landscape?</p>
		<div class="flex three">
			<div>
				<h4>Has a crowd gathered around you?</h4>
				<p>No! You’re trying to find the crowd, probably gathered for some other reason, and create a sideshow that will distract their attention from the main event.</p>
			</div>
			<div>
				<h4>Have they paid money to hear you?</h4>
				<p>No! You’re the one paying low-ROI money to a third party in a vain attempt to lure the crowd away from activities they’re already happily engaged in.</p>
			</div>
			<div>
				<h4>Does the crowd want to hear what you have to say?</h4>
				<p>No! Unless they have an acute problem and you can solve it, everything you say is just a noisy nuisance.</p>
			</div>
		</div>
		<p>But, if you look closely, you’ll notice the opportunity in this scenario: Someone has a problem that you are uniquely positioned to solve.</p>
		<p>So, go find the problems.</p>
		<div class="overflow">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-Audience-2-Desktop.png" alt="Round Boulder on Grass"/>
		</div>
		<section class="hill">
			<h3>Get real and get centered</h3>
			<p>Let’s stop “defining our audience.” Instead, let’s be human-centered. And let’s start by taking apart the words Human and Centered to understand this new posture.</p>
			<div class="flex two">
				<div>
					<p class="large-p"><strong>Human:</strong> of, pertaining to, characteristic of, or having the nature of people</p>
					<p>To be human-centered means being about the very core, heart, and soul of people. It means considering individuals instead of a demographic set. It means knowing what makes them excited, stressed, overjoyed, or tired. It means talking with them, observing their lives, and walking a mile in their shoes. Ultimately, it means understanding their real problem and how they relate to that problem.</p>
				</div>
				<div>
					<p class="large-p"><strong>Centered:</strong> having a central axis</p>
					<p>If these individuals are your central axis, then everything you do revolves around them and their problem. They are truly the center of your universe! You’ll change from touting your technological achievements to describing how you meet your Center’s needs. You’ll reimagine your order fulfillment process to delight your Center instead of doing what’s easiest for your warehouse team. You’ll find new meaning and purpose in your work because you’ll know exactly whom you’re helping, what problem you’re solving, and why.</p>
				</div>
			</div>
		</section>
		<div class="flex">
			<div>
				<h3>Speak clearly</h3>
				<p>When it comes to your branding and marketing—all those lovely things you wish to say about yourself and your product—speak to your Center, and only to your Center. Don’t build a brand that has broad appeal; be narrow and precise about how you’re delivering exactly what your Center has been looking for. Position yourself as the specific answer to a specific problem for your specific Center.</p>
				<br>
				<img class="mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-Audience-4-Desktop.png" alt="Boulder Got you Down? We Can Help! Sisyphus Greek Scroll" />
				<p class="tenso-regular">Two pro tips about “centered” messaging:</p>
				<p class="tenso-regular has-small-font-size">1) Rewrite the mass appeal of your messaging to be a subculture-focused, niche-specific dialect that cuts through the clutter.</p>
				<p class="tenso-regular has-small-font-size">2) If it doesn’t matter to your Center, it doesn’t matter to you. For example, no one cares how a car works; they care that it’s comfortable, that it can carry their kayaks, and that it looks damn good in blue. Speak only to the things that matter to your Center.</p>
			</div>
			<img class="desktop" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-Audience-4-Desktop.png" alt="Boulder Got you Down? We Can Help! Sisyphus Greek Scroll" />
		</div>
		<div class="right-p">
			<h3>Listen up</h3>
			<p>You’ve gotten centered—nice! But finding your Center is not a single step, it’s an ongoing journey. Your Center will evolve and change, and your empathetic support of your Center must adapt right along with them.</p>
			<p>Initiate both micro and macro feedback loops. Listen to your people in the moment, and then listen to them as they move through their life journeys. What you’ll hear will surprise you… if you’re listening closely, it will always reveal your best next move.</p>
		</div>
		<section class="single-image">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-Audience-6-Desktop.png" alt="Sisyphus Relaxing on Hammock with Boulder" />
		</section>
		<h3>Find your people</h3>
		<p>We were once working with a client who designs a very fun line of impulse-purchase items found in your typical big-box retailer. Because of the nature of impulse buying, the demography of the “target audience” was impossible to nail down.</p>
		<p>While workshopping this problem with our client, their Sales Director recalled a recent observation she’d made in store. She watched a few shoppers walk their products without a second glance. They were too busy, too distracted, or too stuffy to notice.</p>
		<p>“But then a woman walked past wearing gold sparkly shoes and pushing a full shopping cart that looked like she was prepping for a boozy brunch. And I thought, ‘She’s our gal!’ Sure enough, she stopped, doubled back, and grabbed one of everything.”</p>
		<p>Stop defining your audience in a conference room. Go find your sparkly shoe lady.</p>
		<br>
		<br>
		<div class="relative">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/19-SKI-CoreContent-Audience-7-Desktop.png" alt="Sisyphus Greek Sandals"/>
		</div>
		<br>
		<br>
		<br>
	</article>
</main>

<?php get_template_part('template-parts/sections/further-reading'); ?>

<?php get_template_part('template-parts/sections/related-approach'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>