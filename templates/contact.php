<?php 
/*----------------------------------------------------------------*\

	Template Name: Contact

\*----------------------------------------------------------------*/
?>
<?php 
	$phone_link = get_field('primary_phone_number','options');
	$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<div class="blog-wrap">
	<main id="main-content">
		<article>
			<?php the_content(); ?>
		</article>
	</main>
	<aside>
		<h2 class="h6">Visit us</h2>
		<hr>
		<p><?php the_field('primary_address','options'); ?><br><?php the_field('primary_city','options'); ?>, <?php the_field('primary_state','options'); ?> <?php the_field('primary_zip','options'); ?></p>
		<h2 class="h6">Call us</h2>
		<hr>
		<a href="tel:+1<?php echo $phone_link; ?>"><?php the_field('primary_phone_number','options'); ?></a>
		<h2 class="h6">Be our friend</h2>
		<hr>
		<div>
			<a href="<?php the_field('dribbble','options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#dribbble" />
				</svg>
			</a>
			<a href="<?php the_field('instagram','options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#instagram" />
				</svg>
			</a>
			<a href="<?php the_field('linkedin','options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#linkedin" />
				</svg>
			</a>
		</div>
	</aside>
</div>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>