<?php 
/*----------------------------------------------------------------*\

		DEFAULT TAXONOMY TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>
<?php 
	$taxonomy = get_queried_object();
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<div class="blog-wrap">
	<header class="post-head">
		<?php if ( get_queried_object() ) : ?>
			<h1><?php echo  $taxonomy->name; ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
	</header>

	<aside>
		<hr>
		<form role="search" method="get" action="<?php echo get_site_url(); ?>">
			<label>Search</label>
			<input type="search" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
			<button type="submit">Search</button>
		</form>
		<h6>Topics</h6>
		<hr>
		<ul>
			<?php 
			$categories = get_categories( array(
				'orderby' => 'name',
				'parent'  => 0
			) );
			?>
			<?php foreach ( $categories as $category ) { ?>
				<li>
					<a href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>"><?php echo $category->name; ?></a>
				</li>
			<?php } ?>
		</ul>
	</aside>

	<main id="main-content">
		<article class="feed">
			<?php	while ( have_posts() ) : the_post(); ?>
				<article class="archive-result <?php echo $post_type; ?>">
					<header>
						<?php	
						$categories = get_the_category();
						if ( ! empty( $categories ) ) {
							echo '<a class="category" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
						} ?>
						<hr>
						<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
						<?php	
						$author_id = get_the_author_meta('ID');
						?>
						<p><span><?php the_date('F j Y'); ?></span> / <?php the_author(); ?><?php if (get_field('position', 'user_'. $author_id)) : ?>, <?php the_field('position', 'user_'. $author_id); ?><?php endif; ?></p>
					</header>
					<div class="entry-content">
						<?php the_excerpt(); ?>
					</div>
				</article>
			<?php endwhile; ?>
		</article>
		<section class="infinite-scroll is-standard-width has-small-spacing">
			<div class="page-load-status">
				<p class="infinite-scroll-request">
					<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
						<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
							<animateTransform attributeType="xml"
								attributeName="transform"
								type="rotate"
								from="0 25 25"
								to="360 25 25"
								dur="0.6s"
								repeatCount="indefinite"/>
						</path>
					</svg>
				</p>
				<p class="infinite-scroll-last"></p>
				<p class="infinite-scroll-error"></p>
			</div>
			<?php the_posts_pagination( array(
				'prev_text'	=> __( 'Previous page' ),
				'next_text'	=> __( 'Next page' ),
			) ); ?>
			<button class="load-more">View more</button>
		</section>
	</main>
</div>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>