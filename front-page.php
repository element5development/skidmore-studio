<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/home-header'); ?>

<main id="main-content">
	<article>
		<?php the_content(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/photo-scatter'); ?>

<?php get_template_part('template-parts/sections/brands'); ?>

<?php get_template_part('template-parts/sections/home-reach-out'); ?>

<?php get_template_part('template-parts/sections/four-columns'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>