<?php 
/*----------------------------------------------------------------*\

		WORK ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article class="grid">
		<?php	while ( have_posts() ) : the_post(); ?>
			<article class="preview">
				<a href="<?php the_permalink(); ?>">
					<?php $image = get_field('thumbnail'); ?>
					<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
					<div>
						<p>Case Study</p>
						<h2><?php the_title(); ?></h2>
					</div>
				</a>
			</article>
		<?php endwhile; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>