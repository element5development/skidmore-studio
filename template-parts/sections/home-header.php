<?php 
/*----------------------------------------------------------------*\

		HOME HEADER

\*----------------------------------------------------------------*/
?>
<?php $introvideo = get_field('header_intro_video'); ?>
<?php $loopingvideo = get_field('header_looping_video'); ?>
<?php $headerimage = get_field('header_image'); ?>

<header class="home-head">
	<div>
		<video muted="" autoplay="" playsinline="" class="video intro" src="<?php echo get_site_url(); ?><?php echo $introvideo['url']; ?>"></video>
		<video muted="" autoplay="" loop="" playsinline="" class="video" poster="<?php echo $headerimage['url']; ?>" src="<?php echo get_site_url(); ?><?php echo $loopingvideo['url']; ?>"></video>
	</div>
</header>