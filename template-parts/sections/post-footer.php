<?php 
/*----------------------------------------------------------------*\

		POST FOOTER
		Display copyright and navigation

\*----------------------------------------------------------------*/
?>
<?php 
	$phone_link = get_field('primary_phone_number','options');
	$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
?>

<footer class="post-footer">
	<div>
		<div>
			<?php echo do_shortcode('[gravityform id="4" title="true" description="true" ajax="true"]') ?>
		</div>
		<?php wp_nav_menu(array( 'theme_location' => 'footer_navigation' )); ?>
		<div>
			<a href="tel:+1<?php echo $phone_link; ?>"><?php the_field('primary_phone_number','options'); ?></a>
			<address><?php the_field('primary_address','options'); ?><br><?php the_field('primary_city','options'); ?>, <?php the_field('primary_state','options'); ?> <?php the_field('primary_zip','options'); ?></address>
			<div>
				<a href="<?php the_field('dribbble','options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#dribbble" />
					</svg>
				</a>
				<a href="<?php the_field('instagram','options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#instagram" />
					</svg>
				</a>
				<a href="<?php the_field('linkedin','options'); ?>" target="_blank">
					<svg>
						<use xlink:href="#linkedin" />
					</svg>
				</a>
			</div>
		</div>
		<hr>
		<a href="<?php echo get_site_url(); ?>">
			<svg>
				<use xlink:href="#footer-logo" />
			</svg>
		</a>
		<p>
			<span>Serious branding.</span>
			<span>©<?php echo date('Y'); ?> Skidmore Studio | <a href="/privacy-policy/">Privacy</a></span>
		</p>
	</div>
</footer>