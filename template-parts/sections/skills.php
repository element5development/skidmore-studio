<?php 
/*----------------------------------------------------------------*\
	SKILLS
\*----------------------------------------------------------------*/
?>

<section class="skills">
	<div>
		<button class="main">
			<h3 class="h4"><?php the_field('skills_title'); ?></h3>
			<div class="plus-to-minus"></div>
		</button>
		<hr>
		<div class="toggle">
			<div>
				<button class="people">
					<h5>People</h5>
					<div class="plus-to-minus"></div>
				</button>
				<hr>

				<?php if( have_rows('people_list') ): ?>
					<ul class="people">
					<?php while ( have_rows('people_list') ) : the_row(); ?>
						<li><?php the_sub_field('people'); ?></li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>

			</div>
			<div>
				<button class="industries">
					<h5>Industries</h5>
					<div class="plus-to-minus"></div>
				</button>
				<hr>

				<?php if( have_rows('industries_list') ): ?>
					<ul class="industries">
					<?php while ( have_rows('industries_list') ) : the_row(); ?>
						<li><?php the_sub_field('industry'); ?></li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>

			</div>
			<div>
				<button class="services">
					<h5>Services</h5>
					<div class="plus-to-minus"></div>
				</button>
				<hr>

				<?php if( have_rows('services_list') ): ?>
					<ul class="services">
					<?php while ( have_rows('services_list') ) : the_row(); ?>
						<li><?php the_sub_field('service'); ?></li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>

			</div>
		</div>
	</div>
</section>