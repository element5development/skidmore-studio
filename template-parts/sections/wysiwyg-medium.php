<?php 
/*----------------------------------------------------------------*\
	WYSIWYG MEDIUM
\*----------------------------------------------------------------*/
?>

<section class="wysiwyg medium-width">
	<div>
		<?php the_sub_field('wysiwyg_medium'); ?>
	</div>
</section>