<?php 
/*----------------------------------------------------------------*\
	WYSIWYG SMALL
\*----------------------------------------------------------------*/
?>

<section class="wysiwyg small-width">
	<div>
		<?php the_sub_field('wysiwyg_small'); ?>
	</div>
</section>