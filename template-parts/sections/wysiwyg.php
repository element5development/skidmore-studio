<?php 
/*----------------------------------------------------------------*\
	WYSIWYG FULL
\*----------------------------------------------------------------*/
?>

<section class="wysiwyg full-width">
	<div>
		<?php the_sub_field('wysiwyg_full'); ?>
	</div>
</section>