<?php 
/*----------------------------------------------------------------*\
	PHOTO SCATTER
\*----------------------------------------------------------------*/
?>

<section class="photo-scatter">
	<div>
		<?php $image = get_field('image_1'); ?>
		<?php $link = get_field('image_1_link'); ?>
		<?php if ($image) : ?>
			<a class="paroller" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" data-paroller-factor="0.1" data-paroller-type="foreground">
				<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" />
			</a>
		<?php endif; ?>

		<?php $image2 = get_field('image_2'); ?>
		<?php $link2 = get_field('image_2_link'); ?>
		<?php if ($image2) : ?>
			<a class="paroller" href="<?php echo $link2['url']; ?>" target="<?php echo $link2['target']; ?>" data-paroller-factor="0.15" data-paroller-type="foreground">
				<img src="<?php echo $image2['sizes']['medium']; ?>" alt="<?php echo $image2['alt'] ?>" />
			</a>
		<?php endif; ?>

		<?php $image3 = get_field('image_3'); ?>
		<?php $link3 = get_field('image_3_link'); ?>
		<?php if ($image3) : ?>
			<a class="paroller" href="<?php echo $link3['url']; ?>" target="<?php echo $link3['target']; ?>" data-paroller-factor="0.3" data-paroller-factor-xs="0.1" data-paroller-type="foreground">
				<img src="<?php echo $image3['sizes']['medium']; ?>" alt="<?php echo $image3['alt'] ?>" />
			</a>
		<?php endif; ?>

		<?php $image4 = get_field('image_4'); ?>
		<?php $link4 = get_field('image_4_link'); ?>
		<?php if ($image4) : ?>
			<a class="paroller" href="<?php echo $link4['url']; ?>" target="<?php echo $link4['target']; ?>" data-paroller-factor="0.2" data-paroller-type="foreground">
				<img src="<?php echo $image4['sizes']['medium']; ?>" alt="<?php echo $image4['alt'] ?>" />
			</a>
		<?php endif; ?>
	</div>
</section>