<?php 
/*----------------------------------------------------------------*\
	WYSIWYG
\*----------------------------------------------------------------*/
?>

<section class="related-work">
	<div>

		<?php $posts = get_field('related_posts'); ?>
		<?php if( $posts ): ?>
			<?php	foreach( $posts as $post): ?>
				<article class="preview">
					<a href="<?php the_permalink(); ?>">
						<?php $image = get_field('thumbnail'); ?>
						<div class="image-wrap">
							<div style="background-image: url('<?php echo $image['sizes']['medium']; ?>');"></div>
						</div>
						<div class="hover">
							<h2><?php the_title(); ?></h2>
						</div>
					</a>
				</article>
			<?php endforeach; ?>
		<?php endif; ?>
		
	</div>
</section>