<?php 
/*----------------------------------------------------------------*\
	BRANDS
\*----------------------------------------------------------------*/
?>

<section class="brands">
	<div>
		<h4><?php the_field('brands_title'); ?></h4>

		<?php if( have_rows('brand_repeater') ): ?>
			<?php while ( have_rows('brand_repeater') ) : the_row(); ?>
				<div>
					<?php $logo = get_sub_field('brand'); ?>
					<img class="<?php the_sub_field('class'); ?>" src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt'] ?>" />
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>