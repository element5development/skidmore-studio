<?php 
/*----------------------------------------------------------------*\

		PAGE HEADER
		Display the page title

\*----------------------------------------------------------------*/
?>

<header class="page-head">
	<?php if ( get_field('title') ) : ?>
		<?php the_field('title'); ?>
	<?php else : ?>
		<h1><?php the_title(); ?></h1>
	<?php endif; ?>
</header>