<?php 
/*----------------------------------------------------------------*\
	TWO IMAGE CONTENT RIGHT
\*----------------------------------------------------------------*/
?>

<section class="two-image-content-right">
	<div>
		<div>
			<?php the_sub_field('content_upper_right'); ?>
		</div>

		<?php $image = get_sub_field('image_1'); ?>
		<?php if ($image) : ?>
			<img class="paroller" src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" data-paroller-factor="<?php the_sub_field('image_1_parallax_speed_and_direction'); ?>" data-paroller-type="foreground" />
		<?php endif; ?>

		<?php $image2 = get_sub_field('image_2'); ?>
		<?php if ($image2) : ?>
		<img class="paroller" src="<?php echo $image2['sizes']['medium']; ?>" alt="<?php echo $image2['alt'] ?>" data-paroller-factor="<?php the_sub_field('image_2_parallax_speed_and_direction'); ?>" data-paroller-type="foreground" />
		<?php endif; ?>
	</div>
</section>