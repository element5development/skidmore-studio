<?php 
/*----------------------------------------------------------------*\
	VALUES
\*----------------------------------------------------------------*/
?>

<section class="values">
	<div>
		<h3 class="h4"><?php the_field('values_title'); ?></h3>

		<?php if( have_rows('value_repeater') ): ?>
			<div>
			<?php while ( have_rows('value_repeater') ) : the_row(); ?>
				<article>
					<p class="is-large"><?php the_sub_field('number'); ?></p>
					<h3><?php the_sub_field('value'); ?></h3>
					<p class="miller-text"><?php the_sub_field('description'); ?></p>
				</article>
			<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</div>
</section>