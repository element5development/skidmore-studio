<?php 
/*----------------------------------------------------------------*\
	HOME REACH OUT
\*----------------------------------------------------------------*/
?>

<section class="home-reach-out">
	<div>
		<?php $mainimage = get_field('reach_out_image'); ?>
		<img src="<?php echo $mainimage['sizes']['large']; ?>" alt="<?php echo $mainimage['alt'] ?>" />
		<div class="paroller" data-paroller-factor="-0.05" data-paroller-factor-sm="0.0001" data-paroller-type="foreground">
			<h2 class="h1">Got a project for us?</h2>
			<a class="button" href="<?php echo get_site_url(); ?>/contact/">Let's talk</a>
		</div>
	</div>
</section>