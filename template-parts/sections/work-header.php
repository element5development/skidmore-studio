<?php 
/*----------------------------------------------------------------*\
	Work Header
\*----------------------------------------------------------------*/
?>

<header class="work-head">
	<?php $image = get_field('featured_image'); ?>
	<div style="background-image: url('<?php echo $image['sizes']['xlarge']; ?>');">
		<div class="paroller" data-paroller-factor="0.1" data-paroller-type="foreground">
			<h1><?php the_title(); ?></h1>
			<?php if( have_rows('service_repeater') ): ?>
				<?php while ( have_rows('service_repeater') ) : the_row(); ?>
					<p><?php the_sub_field('service'); ?></p>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</header>