<?php 
/*----------------------------------------------------------------*\
	LOGO GRID
\*----------------------------------------------------------------*/
?>

<section class="logo-grid">
	<div>
		<?php if( have_rows('logo_repeater') ): ?>
			<?php while ( have_rows('logo_repeater') ) : the_row(); ?>
				<div>
					<?php $logo = get_sub_field('logo'); ?>
					<img src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt'] ?>" />
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>