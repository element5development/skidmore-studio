<?php 
/*----------------------------------------------------------------*\
	THE UPSHOT
\*----------------------------------------------------------------*/
?>

<section id="results" class="the-upshot">
	<div>
		<div>
			<h3>The results</h3>
			<?php the_sub_field('the_upshot'); ?>
		</div>
		<?php $upshotimage = get_sub_field('upshot_image'); ?>
		<img src="<?php echo $upshotimage['sizes']['large']; ?>" alt="<?php echo $upshotimage['alt'] ?>" />
	</div>
</section>