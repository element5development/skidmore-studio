<?php 
/*----------------------------------------------------------------*\
	RELATED APPROACH
\*----------------------------------------------------------------*/
?>

<section class="related-approach">
	<div>

		<?php $posts = get_field('related_posts'); ?>
		<?php if( $posts ): ?>
			<?php	foreach( $posts as $post): ?>
				<article class="preview">
					<a href="<?php the_permalink(); ?>"></a>
					<h2><?php the_field('title'); ?></h2>
					<p><?php the_field('description'); ?></p>
					<div>
						<?php $image = get_field('thumbnail'); ?>
						<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</article>
			<?php endforeach; ?>
		<?php endif; ?>
		
	</div>
</section>