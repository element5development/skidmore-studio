<?php 
/*----------------------------------------------------------------*\
	QUOTE
\*----------------------------------------------------------------*/
?>

<section class="quote">
	<div>
		<h3><?php the_sub_field('quote'); ?></h3>
		<p><?php the_sub_field('author'); ?></p>
	</div>
</section>