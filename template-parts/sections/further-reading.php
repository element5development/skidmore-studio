<?php 
/*----------------------------------------------------------------*\
	FURTHER READING
\*----------------------------------------------------------------*/
?>

<section class="further-reading">
	<div>
		<h3>Further Reading</h3>
		<?php if( have_rows('further_reading') ): ?>
			<?php while ( have_rows('further_reading') ) : the_row(); ?>
				<article>
					<?php $link = get_sub_field('link'); ?>
					<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
						<p><strong><?php the_sub_field('title'); ?></strong></p>
						<p><?php the_sub_field('description'); ?></p>
					</a>
				</article>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>