<?php 
/*----------------------------------------------------------------*\
	TEAM
\*----------------------------------------------------------------*/
?>

<section class="team">
	<div>
		<h3 class="h4"><?php the_field('team_title'); ?></h3>

		<?php if( have_rows('team_repeater') ): ?>
			<div>
				<div>
				<?php while ( have_rows('team_repeater') ) : the_row(); ?>
					<article>
						<?php $polaroid = get_sub_field('polaroid'); ?>
						<img src="<?php echo $polaroid['sizes']['small']; ?>" alt="<?php echo $polaroid['alt']; ?>" />
						<h4><?php the_sub_field('name'); ?></h4>
						<p><?php the_sub_field('position'); ?></p>
					</article>
				<?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
		
	</div>
</section>