<?php 
/*----------------------------------------------------------------*\
	REACH OUT
\*----------------------------------------------------------------*/
?>

<section class="reach-out">
	<div>
		<h2>Let's get to work.</h2>
		<a class="button" href="<?php echo get_site_url(); ?>/contact/">Reach out</a>
	</div>
</section>