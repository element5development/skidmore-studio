<?php 
/*----------------------------------------------------------------*\
	STUDIO
\*----------------------------------------------------------------*/
?>

<section class="studio">
	<div>
		<h3 class="h4"><?php the_field('studio_title'); ?></h3>
		<p><?php the_field('studio_description'); ?></p>
		<?php $mainimage = get_field('main_image'); ?>
		<img src="<?php echo $mainimage['sizes']['large']; ?>" alt="<?php echo $mainimage['alt']; ?>" />

		<div>
			<?php $image = get_field('image_1'); ?>
			<?php if ($image) : ?>
			<img class="paroller" src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt'] ?>" data-paroller-factor="0.04" data-paroller-type="foreground" />
			<?php endif; ?>

			<?php $image2 = get_field('image_2'); ?>
			<?php if ($image2) : ?>
			<img class="paroller" src="<?php echo $image2['sizes']['medium']; ?>" alt="<?php echo $image2['alt'] ?>" data-paroller-factor="0.02" data-paroller-type="foreground" />
			<?php endif; ?>

			<?php $image3 = get_field('image_3'); ?>
			<?php if ($image3) : ?>
			<img class="paroller" src="<?php echo $image3['sizes']['medium']; ?>" alt="<?php echo $image3['alt'] ?>" data-paroller-factor="0.06" data-paroller-type="foreground" />
			<?php endif; ?>
		</div>
	</div>
</section>