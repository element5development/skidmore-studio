<?php 
/*----------------------------------------------------------------*\
	FOUR COLUMNS
\*----------------------------------------------------------------*/
?>

<section class="four-columns">
	<div>
		<div>
			<p class="has-small-font-size"><?php the_field('column_1_title'); ?></p>
			<p><span id="temp"></span>° and <span id="description"></span>.</p>
		</div>
		<div>
			<p class="has-small-font-size"><?php the_field('column_2_title'); ?></p>
			<?php 
			$rows = get_field('column_2_song_list' ); // get all the rows
			$rand_row = $rows[ array_rand( $rows ) ]; // get a random row
			$rand_row_song = $rand_row['song_link' ]; // get the sub field value 
			?>
			<?php if ($rand_row_song) : ?>
				<p><a href="<?php echo $rand_row_song['url']; ?>" target="<?php echo $rand_row_song['target']; ?>"><?php echo $rand_row_song['title']; ?></a></p>
			<?php endif; ?>
		</div>
		<div>
			<p class="has-small-font-size"><?php the_field('column_3_title'); ?></p>
			<?php the_field('column_3_description'); ?>
		</div>
		<div>
			<p class="has-small-font-size"><?php the_field('column_4_title'); ?></p>
			<?php 
			$sentencerows = get_field('column_4_random_sentences' ); // get all the rows
			$sentencerand_row = $sentencerows[ array_rand( $sentencerows ) ]; // get a random row
			$rand_row_sentence = $sentencerand_row['sentence' ]; // get the sub field value 
			?>
			<?php if ($rand_row_sentence) : ?>
				<p><?php echo $rand_row_sentence; ?></p>
			<?php endif; ?>
		</div>
	</div>
</section>