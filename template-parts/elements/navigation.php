<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo" />
			</svg>
		</a>
		<button>
			<div class="hamburger-menu"></div>
		</button>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
		<?php wp_nav_menu(array( 'theme_location' => 'mobile_navigation' )); ?>
	</nav>
</div>
<a class="wings" href="<?php echo get_home_url(); ?>">
	<svg>
		<use xlink:href="#wing-1" />
	</svg>
</a>
<a class="wings" href="<?php echo get_home_url(); ?>">
	<svg>
		<use xlink:href="#wing-2" />
	</svg>
</a>