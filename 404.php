<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">

	<h1><span>404</span>We're ever so sorry, but something's gone terribly wrong.</h1>
</header>

<main id="main-content">
	<article>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/404.png" alt="We're ever so sorry, but something's gone terribly wrong."/>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>