var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		MOBILE NAV
	\*----------------------------------------------------------------*/
	$(".primary-navigation nav button").on('click', function () {
		$(".hamburger-menu").toggleClass("is-active");
		$('#menu-mobile-navigation').toggleClass("is-active");
		$('html, body').toggleClass("is-active");
	});

	document.addEventListener('touchmove', function (e) {
		if ($(document.documentElement).hasClass('is-active')) {
			console.log('touchmove prevent');
			e.preventDefault();
		}
	}, {
		passive: false
	});

	/*----------------------------------------------------------------*\
		SHOW NAV ON SCROLL UP
	\*----------------------------------------------------------------*/
	$(window).scroll(function () {
		var currentTop = $(window).scrollTop();
		if (currentTop <= 0) {
			$(".primary-navigation").removeClass('is-visible');
			$(".primary-navigation").removeClass('not-visible');
		} else if (currentTop < this.previousTop) {
			$(".primary-navigation").removeClass('not-visible');
			$(".primary-navigation").addClass('is-visible');
		} else {
			$(".primary-navigation").addClass('not-visible');
			$(".primary-navigation").removeClass('is-visible');
		}
		this.previousTop = currentTop;
	});

	/*----------------------------------------------------------------*\
  	MAKE WINGS VISIBLE ON SCROLL
	\*----------------------------------------------------------------*/
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();

		if (scroll + $(window).height() > $(document).height() - 500) {
			$('.wings').removeClass('visible');
		} else if (scroll === 0) {
			$('.wings').removeClass('visible');
		} else if (scroll >= 150) {
			$('.wings').addClass('visible');
		}
	});

	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('.ginput_container_fileupload').addClass("file-uploaded");
		});
	}

	/*------------------------------------------------------------------
		MASONRY FOR WP GALLERY
	------------------------------------------------------------------*/
	$('.landscape').parent().addClass("landscape");
	var galleries = document.querySelectorAll('.gallery');
	for (var i = 0, len = galleries.length; i < len; i++) {
		var gallery = galleries[i];
		initMasonry(gallery);
	}

	function initMasonry(container) {
		var imgLoad = imagesLoaded(container, function () {
			new Masonry(container, {
				itemSelector: '.gallery-item',
				columnWidth: '.gallery-item:nth-child(4n-1)',
				percentPosition: true,
				horizontalOrder: true,
			});
		});
	}

	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'block');
	}

	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.archive-result',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});

	/*------------------------------------------------------------------
  	ACTIVE CATEGORY
	------------------------------------------------------------------*/
	var loc = window.location.href;
	$(".blog-wrap > aside > ul a").each(function () {
		if (loc.indexOf($(this).attr("href")) != -1) {
			$(this).closest('a').addClass("is-active");
		}
	});

	/*------------------------------------------------------------------
  	MASONRY FOR WORK
	------------------------------------------------------------------*/
	var $grid = $('.grid');
	// init Masonry
	$grid.masonry({
		itemSelector: 'article.preview',
	});
	// layout Masonry after each image loads
	$grid.imagesLoaded().progress(function () {
		$grid.masonry();
	});

	/*------------------------------------------------------------------
  	STYLED SELECT
	------------------------------------------------------------------*/
	var x, i, j, selElmnt, a, b, c;
	/* Look for any elements with the class "ginput_container_select": */
	x = document.getElementsByClassName("ginput_container_select");
	for (i = 0; i < x.length; i++) {
		selElmnt = x[i].getElementsByTagName("select")[0];
		/* For each element, create a new DIV that will act as the selected item: */
		a = document.createElement("DIV");
		a.setAttribute("class", "select-selected");
		a.setAttribute("tabindex", "0");
		a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
		x[i].appendChild(a);
		/* For each element, create a new DIV that will contain the option list: */
		b = document.createElement("DIV");
		b.setAttribute("class", "select-items select-hide");
		for (j = 1; j < selElmnt.length; j++) {
			/* For each option in the original select element,
			create a new DIV that will act as an option item: */
			c = document.createElement("DIV");
			c.innerHTML = selElmnt.options[j].innerHTML;
			c.addEventListener("click", function (e) {
				/* When an item is clicked, update the original select box,
				and the selected item: */
				var y, i, k, s, h;
				s = this.parentNode.parentNode.getElementsByTagName("select")[0];
				h = this.parentNode.previousSibling;
				for (i = 0; i < s.length; i++) {
					if (s.options[i].innerHTML == this.innerHTML) {
						s.selectedIndex = i;
						h.innerHTML = this.innerHTML;
						y = this.parentNode.getElementsByClassName("same-as-selected");
						for (k = 0; k < y.length; k++) {
							y[k].removeAttribute("class");
						}
						this.setAttribute("class", "same-as-selected");
						break;
					}
				}
				h.click();
			});
			b.appendChild(c);
		}
		x[i].appendChild(b);

		a.addEventListener("click", function (e) {
			/* When the select box is clicked, close any other select boxes,
			and open/close the current select box: */
			e.stopPropagation();
			closeAllSelect(this);
			this.nextSibling.classList.toggle("select-hide");
			this.classList.toggle("select-arrow-active");
		});
	}

	function closeAllSelect(elmnt) {
		/* A function that will close all select boxes in the document,
		except the current select box: */
		var x, y, i, arrNo = [];
		x = document.getElementsByClassName("select-items");
		y = document.getElementsByClassName("select-selected");
		for (i = 0; i < y.length; i++) {
			if (elmnt == y[i]) {
				arrNo.push(i)
			} else {
				y[i].classList.remove("select-arrow-active");
			}
		}
		for (i = 0; i < x.length; i++) {
			if (arrNo.indexOf(i)) {
				x[i].classList.add("select-hide");
			}
		}
	}

	/* If the user clicks anywhere outside the select box,
	then close all select boxes: */
	document.addEventListener("click", closeAllSelect);

	$('.select-selected').on('click', function () {
		var selected = $('.select-selected').text();
		$('.gfield_select').val(selected).change();
	});

	$('.select-selected').on('keypress', function () {
		if (e.which == 13) { //Enter key pressed
			var selected = $('.select-selected').text();
			$('.gfield_select').val(selected).change();
		}
	});

	/*------------------------------------------------------------------
  	PLUS TO MINUS
	------------------------------------------------------------------*/
	$('section.skills button.main').on('click', function () {
		$(this).toggleClass('is-open');
		$('.toggle').toggleClass('is-open');
	});
	$('section.skills button.people').on('click', function () {
		$(this).toggleClass('is-open');
		$('div.toggle > div ul.people').toggleClass('is-open');
	});
	$('section.skills button.industries').on('click', function () {
		$(this).toggleClass('is-open');
		$('div.toggle > div ul.industries').toggleClass('is-open');
	});
	$('section.skills button.services').on('click', function () {
		$(this).toggleClass('is-open');
		$('div.toggle > div ul.services').toggleClass('is-open');
	});

	/*------------------------------------------------------------------
  	GET CURRENT WEATHER
	------------------------------------------------------------------*/
	if ($('body').hasClass('home')) {
		$.ajax({
			type: "GET",
			url: "https://api.openweathermap.org/data/2.5/weather?q=Detroit&units=imperial&APPID=a5cc1a8fb73588b03cfcc81c25bd5236",
			success: function (weather) {
				console.log(weather);
				console.log(weather.main.temp);
				console.log(weather.weather[0].main);
				console.log(weather.weather[0].description);
				var temp = weather.main.temp;
				var rounded = Math.round(temp);
				var description = weather.weather[0].description;
				var thetemp = document.getElementById('temp');
				var thedescription = document.getElementById('description');
				thetemp.textContent = rounded;
				thedescription.textContent = description;
			},
			dataType: "json"
		});
	}

	/*------------------------------------------------------------------
  	PARALLAX SECTIONS
	------------------------------------------------------------------*/
	$('.paroller').paroller();

	/*------------------------------------------------------------------
  	VIDEO SECTIONS
	------------------------------------------------------------------*/
	$('.video.intro').show('fast').delay(5000).fadeOut('fast', function () {
		$(this).remove();
	});

});