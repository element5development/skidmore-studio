<?php 
/*----------------------------------------------------------------*\

	WORK SINGLE POST TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php if ( ! post_password_required() ) { ?>

<?php get_template_part('template-parts/sections/work-header'); ?>

<main id="main-content">
	<article>
		<?php if( have_rows('content') ):
			while ( have_rows('content') ) : the_row();
				if( get_row_layout() == 'wysiwyg_small' ):
					get_template_part('template-parts/sections/wysiwyg-small');
				elseif( get_row_layout() == 'wysiwyg_medium' ): 
					get_template_part('template-parts/sections/wysiwyg-medium');
				elseif( get_row_layout() == 'wysiwyg_full' ): 
					get_template_part('template-parts/sections/wysiwyg');
				elseif( get_row_layout() == 'two_image_content_right' ): 
					get_template_part('template-parts/sections/two-image-content-right');
				elseif( get_row_layout() == 'two_image_content_left' ): 
					get_template_part('template-parts/sections/two-image-content-left');
				elseif( get_row_layout() == 'three_image_content_right' ): 
					get_template_part('template-parts/sections/three-image-content-right');
				elseif( get_row_layout() == 'three_image_content_left' ): 
					get_template_part('template-parts/sections/three-image-content-left');
				elseif( get_row_layout() == 'logo_grid' ): 
					get_template_part('template-parts/sections/logo-grid');
				elseif( get_row_layout() == 'the_upshot' ): 
					get_template_part('template-parts/sections/the-upshot');
				elseif( get_row_layout() == 'quote' ): 
					get_template_part('template-parts/sections/quote');
				endif;
			endwhile;
		endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/related-work'); ?>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php
	}else{
		echo get_the_password_form($post->ID);
	}
?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>