<?php

/*----------------------------------------------------------------*\
		INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_navigation' => __( 'Primary Menu' ),
		'mobile_navigation' => __( 'Mobile Menu' ),
		'footer_navigation' => __( 'Footer Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );

/*----------------------------------------------------------------*\
	ENABLE YOAST BREADCRUMBS
\*----------------------------------------------------------------*/
add_theme_support( 'yoast-seo-breadcrumbs' );