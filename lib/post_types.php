<?php
/*----------------------------------------------------------------*\
		INITIALIZE POST TYPES
\*----------------------------------------------------------------*/
// Register Custom Post Type Work
function create_work_cpt() {

	$labels = array(
		'name' => _x( 'Works', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Work', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Works', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Work', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Work Archives', 'textdomain' ),
		'attributes' => __( 'Work Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Work:', 'textdomain' ),
		'all_items' => __( 'All Works', 'textdomain' ),
		'add_new_item' => __( 'Add New Work', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Work', 'textdomain' ),
		'edit_item' => __( 'Edit Work', 'textdomain' ),
		'update_item' => __( 'Update Work', 'textdomain' ),
		'view_item' => __( 'View Work', 'textdomain' ),
		'view_items' => __( 'View Works', 'textdomain' ),
		'search_items' => __( 'Search Work', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Work', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Work', 'textdomain' ),
		'items_list' => __( 'Works list', 'textdomain' ),
		'items_list_navigation' => __( 'Works list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Works list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Work', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-portfolio',
		'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'work', $args );

}
add_action( 'init', 'create_work_cpt', 0 );

// Register Custom Post Type Approach
function create_approach_cpt() {

	$labels = array(
		'name' => _x( 'Approaches', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Approach', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Approaches', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Approach', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Approach Archives', 'textdomain' ),
		'attributes' => __( 'Approach Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Approach:', 'textdomain' ),
		'all_items' => __( 'All Approaches', 'textdomain' ),
		'add_new_item' => __( 'Add New Approach', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Approach', 'textdomain' ),
		'edit_item' => __( 'Edit Approach', 'textdomain' ),
		'update_item' => __( 'Update Approach', 'textdomain' ),
		'view_item' => __( 'View Approach', 'textdomain' ),
		'view_items' => __( 'View Approaches', 'textdomain' ),
		'search_items' => __( 'Search Approach', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Approach', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Approach', 'textdomain' ),
		'items_list' => __( 'Approaches list', 'textdomain' ),
		'items_list_navigation' => __( 'Approaches list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Approaches list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Approach', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-aside',
		'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'approach', $args );

}
add_action( 'init', 'create_approach_cpt', 0 );