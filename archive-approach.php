<?php 
/*----------------------------------------------------------------*\

		APPROACH ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<?php	while ( have_posts() ) : the_post(); ?>
			<article class="preview">
				<a href="<?php the_permalink(); ?>"></a>
				<h2><?php the_field('title'); ?></h2>
				<p><?php the_field('description'); ?></p>
				<div>
					<?php $image = get_field('thumbnail'); ?>
					<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</article>
		<?php endwhile; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/reach-out'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>